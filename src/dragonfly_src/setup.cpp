/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   d.mayer@itv.rwth-aachen.de                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <iostream>

#include <stdio.h>
#include <math.h>

#include "setup.h"
#include "data.h"
#include "output.h"

//------------------------------------------------------
bool setup(sData *data)
{

   data->lambda = data->alpha;

   sCell *curCell = NULL;
   sFace *curFace = NULL;
   int i, j;

   /////////////////////////////////
   // construct mesh connectivity //
   /////////////////////////////////
   for (int cellId = 0; cellId < data->nCells; cellId++)
   {
      // assign points to cells
      i = cellId % (data->nPointsX - 1);
      j = cellId / (data->nPointsX - 1);
      curCell = &data->cells[cellId];
      curCell->points[XMYM] = &data->points[j * data->nPointsX + i];
      curCell->points[XPYM] = &data->points[j * data->nPointsX + i + 1];
      curCell->points[XMYP] = &data->points[(j + 1) * data->nPointsX + i];
      curCell->points[XPYP] = &data->points[(j + 1) * data->nPointsX + i + 1];

      // assign faces to cells
      curCell->faces[YM] = &data->faces[cellId];
      curCell->faces[YP] = &data->faces[cellId + data->nPointsX - 1];
      curCell->faces[XM] = &data->faces[(data->nPointsX - 1) * data->nPointsY + cellId + j];
      curCell->faces[XP] = &data->faces[(data->nPointsX - 1) * data->nPointsY + cellId + j + 1];

      // assign cells to faces
      curCell->faces[YM]->neighCells[P] = curCell;
      curCell->faces[YP]->neighCells[M] = curCell;
      curCell->faces[XM]->neighCells[P] = curCell;
      curCell->faces[XP]->neighCells[M] = curCell;

      // assign points to faces
      curCell->faces[YM]->points[M] = curCell->points[XMYM];
      curCell->faces[YM]->points[P] = curCell->points[XPYM];
      curCell->faces[YP]->points[M] = curCell->points[XMYP];
      curCell->faces[YP]->points[P] = curCell->points[XPYP];
      curCell->faces[XM]->points[M] = curCell->points[XMYM];
      curCell->faces[XM]->points[P] = curCell->points[XMYP];
      curCell->faces[XP]->points[M] = curCell->points[XPYM];
      curCell->faces[XP]->points[P] = curCell->points[XPYP];

      //assign neighboring cells to cells
      if (i != 0)
      {
         curCell->neighCells[XM] = &data->cells[cellId - 1];
      }
      if (i != data->nPointsX - 2)
      {
         curCell->neighCells[XP] = &data->cells[cellId + 1];
      }
      if (j != 0)
      {
         curCell->neighCells[YM] = &data->cells[cellId - (data->nPointsX - 1)];
      }
      if (j != data->nPointsY - 2)
      {
         curCell->neighCells[YP] = &data->cells[cellId + (data->nPointsX - 1)];
      }

      // assign physical values
      curCell->rho = data->rho;
   }

   /////////////////////////////////////
   // compute face centers and deltas //
   /////////////////////////////////////
   std::cout << "faces" << std::endl;
   for (int fId = 0; fId < data->nFaces; fId++)
   {
      curFace = &data->faces[fId];
      // FIXME
      // hint: use face points
      curFace->dx = curFace->points[P]->x - curFace->points[M]->x; // if p1.x > p0.x
      curFace->dy = curFace->points[P]->y - curFace->points[M]->y;
      curFace->area = sqrt(pow(curFace->dx, 2) + pow(curFace->dy, 2));

      curFace->x = curFace->points[M]->x + (curFace->dx / 2);
      curFace->y = curFace->points[M]->y + (curFace->dy / 2);
   }

   //////////////////////////////////////
   // compute cell centers and volumes //
   //////////////////////////////////////
   std::cout << "cells" << std::endl;
   for (int cellId = 0; cellId < data->nCells; cellId++)
   {
      // FIXME
      // hint 1: assume cartesian mesh (for now)
      // hint 2: use cell points & neighboring face deltas

      curCell = &data->cells[cellId];

      for (int pId = 0; pId < 4; pId++)
      {
         curCell->x += curCell->points[pId]->x;
         curCell->y += curCell->points[pId]->y;
      }

      curCell->x /= 4;
      curCell->y /= 4;

      curCell->dx = curCell->faces[E]->x - curCell->faces[W]->x;
      curCell->dy = curCell->faces[N]->y - curCell->faces[S]->y;

      double h = 1; // height ?
      curCell->vol = (curCell->faces[YM]->dx * curCell->faces[XM]->dy) * h;
   }

   std::cout << "faces2" << std::endl;
   for (int fId = 0; fId < data->nFaces; fId++)
   {
      curFace = &data->faces[fId];
      std::cout << "ID: " << curFace->id << " Type: " << curFace->bType_u << std::endl;
      if (curFace->bType_u == DIRICHLET)
      {
         double signLength = curFace->neighCells[P]->x - curFace->neighCells[M]->x;
         if (signLength == 0)
         {
            signLength = curFace->neighCells[P]->y - curFace->neighCells[M]->y;
         }
         curFace->D = data->lambda / signLength;
         std::cout << "D: " << curFace->D << std::endl;
      }
   }

   //////////////////////////////////////
   //       boundary conditions        //
   //////////////////////////////////////
   for (int cellId = 0; cellId < data->nCells; cellId++)
   {
      curCell = &data->cells[cellId];
   }

   return true;
}

double analyticSolutionX(sData *data, sCell *curCell, double phi_0_x, double phi_l_x)
{
   double lX = curCell->neighCells[XP]->x - curCell->neighCells[XM]->x;
   double PeX = data->rho * 0.5 * (curCell->faces[XM]->u + curCell->faces[XP]->u) * lX;
   double anPhiX = ((exp(PeX * curCell->x / lX) - 1) / (exp(PeX) - 1)) * (curCell->neighCells[XP]->sc - curCell->neighCells[XM]->sc) + curCell->neighCells[XM]->sc;
}