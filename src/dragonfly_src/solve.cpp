/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   d.mayer@itv.rwth-aachen.de                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <iostream>

#include <stdio.h>
#include <math.h>

#include "solve.h"
#include "data.h"
#include "output.h"

//------------------------------------------------------
bool solve(sData *data)
{
	std::cout << "\nCalculation:\n------------\n";

	double curTime = 0;
	sCell *curCell = NULL;
	sFace *curFace = NULL;

	// dump inital fields
	if (!output(data, curTime))
	{
		std::cout << "ERROR while data output...exiting";
		getchar();
		return 1;
	}

	// Time Loop
	while (curTime < (data->maxTime - data->dt))
	{
		// advance time
		curTime += data->dt;
		std::cout << "\r\t Solving ... current Time: " << curTime << " s         \n";

		calcFlux(data);
		computeCellCoeffs(data);

		simpleAlgorithm(data);

		for (int cellId = 0; cellId < data->nCells; cellId++)
		{
			curCell = &data->cells[cellId];

			// FIXME
			// M2: solve scalar transport equation
			/*
			if (curCell->bType_sc == INNERCELL)
			{
				std::cout << "Cell: " << curCell->id << std::endl;
				calcScalar(curCell, data);

				std::cout << "Value: " << curCell->sc << std::endl;
			}
			*/

			// FIXME
			// M3: solve momentum equation for fixed pressure field
			// precomputeMomentumCoeffs(data, data->dt);

			// FIXME
			// M4: SIMPLE solver loop to solve full incompressible NS-equations
			// calcMomentum(data, data->dt);

			// dump fields after time step
			if (!output(data, curTime))
			{
				std::cout << "ERROR while data output...exiting";
				getchar();
				return 1;
			}
		}

	} // end Time loop

	std::cout << "\n";
	return true;
}

double A(double Pe, int method = 2)
{
	//std::cout << "Method: " << method << std::endl;
	switch (method)
	{
	case 0: // Zentrale Differenzen
		return 1 - 0.5 * Pe;

	case 1: // Updwind
		return 1;

	case 2: // Hybrid
		return std::max(0.0, 1.0 - 0.5 * std::abs(Pe));

	case 3: // Potenz
		return std::max(0.0, pow(1.0 - 0.1 * std::abs(Pe), 5));

	case 4: // Exponential
		return std::abs(Pe) / (exp(std::abs(Pe)) - 1);
	}
}

void computeCellCoeffs(sData *data)
{
	sCell *curCell = NULL;
	sFace *eFace = NULL;
	sFace *wFace = NULL;
	sFace *nFace = NULL;
	sFace *sFace = NULL;

	for (int cellId = 0; cellId < data->nCells; cellId++)
	{
		curCell = &data->cells[cellId];

		if (curCell->bType_sc != INNERCELL)
		{
			continue;
		}

		eFace = curCell->faces[E];
		wFace = curCell->faces[W];
		nFace = curCell->faces[N];
		sFace = curCell->faces[S];

		std::cout << "--- Upwind ---" << std::endl;
		std::cout << "PE: " << eFace->Pe << std::endl;
		std::cout << "PW: " << wFace->Pe << std::endl;
		std::cout << "PN: " << nFace->Pe << std::endl;
		std::cout << "PS: " << sFace->Pe << std::endl;
		std::cout << "AE: " << A(eFace->Pe, data->disMethod) << std::endl;
		std::cout << "AW: " << A(wFace->Pe, data->disMethod) << std::endl;
		std::cout << "AN: " << A(nFace->Pe, data->disMethod) << std::endl;
		std::cout << "AS: " << A(sFace->Pe, data->disMethod) << std::endl;
		std::cout << "--- Calc Scalar ---" << std::endl;
		std::cout << "DE: " << eFace->D << std::endl;
		std::cout << "DW: " << wFace->D << std::endl;
		std::cout << "DN: " << nFace->D << std::endl;
		std::cout << "DS: " << sFace->D << std::endl;
		std::cout << "fE: " << -eFace->f * eFace->area << std::endl;
		std::cout << "fW: " << wFace->f * wFace->area << std::endl;
		std::cout << "fN: " << -nFace->f * nFace->area << std::endl;
		std::cout << "fS: " << sFace->f * sFace->area << std::endl;

		curCell->a[E] = eFace->D * eFace->area * A(eFace->Pe, data->disMethod) + std::max(-eFace->f * eFace->area, 0.0);
		curCell->a[W] = wFace->D * wFace->area * A(wFace->Pe, data->disMethod) + std::max(wFace->f * wFace->area, 0.0);
		curCell->a[N] = nFace->D * nFace->area * A(nFace->Pe, data->disMethod) + std::max(-nFace->f * nFace->area, 0.0);
		curCell->a[S] = sFace->D * sFace->area * A(sFace->Pe, data->disMethod) + std::max(sFace->f * sFace->area, 0.0);

		curCell->a[PP] = (curCell->rho * curCell->dx * curCell->dy) / data->dt;
		curCell->a[PP_Tilde] = curCell->a[E] + curCell->a[W] + curCell->a[N] + curCell->a[S] + curCell->a[PP];

		std::cout << "aE: " << curCell->a[E] << std::endl;
		std::cout << "aW: " << curCell->a[W] << std::endl;
		std::cout << "aN: " << curCell->a[N] << std::endl;
		std::cout << "aS: " << curCell->a[S] << std::endl;
	}
}

void computeFaceCoeffs(sData *data)
{
	sFace *curFace = NULL;
	sFace *eFace = NULL;
	sFace *wFace = NULL;
	sFace *nFace = NULL;
	sFace *sFace = NULL;

	for (int fId = 0; fId < data->nFaces; fId++)
	{
		curFace = &data->faces[fId];

		if (curFace->bType_u != DIRICHLET)
		{
			continue;
		}

		if (curFace->dy != 0)
		{
			eFace = curFace->neighCells[P]->faces[E];
			wFace = curFace->neighCells[M]->faces[W];
			nFace = curFace->neighCells[M]->neighCells[N]->faces[E];
			sFace = curFace->neighCells[M]->neighCells[S]->faces[E];
		}
		else if (curFace->dx != 0)
		{
			eFace = curFace->neighCells[M]->neighCells[E]->faces[N];
			wFace = curFace->neighCells[M]->neighCells[W]->faces[N];
			nFace = curFace->neighCells[P]->faces[N];
			sFace = curFace->neighCells[M]->faces[S];
		}

		std::cout << "--- Upwind ---" << std::endl;
		std::cout << "PE: " << eFace->Pe << std::endl;
		std::cout << "PW: " << wFace->Pe << std::endl;
		std::cout << "PN: " << nFace->Pe << std::endl;
		std::cout << "PS: " << sFace->Pe << std::endl;
		std::cout << "AE: " << A(eFace->Pe, data->disMethod) << std::endl;
		std::cout << "AW: " << A(wFace->Pe, data->disMethod) << std::endl;
		std::cout << "AN: " << A(nFace->Pe, data->disMethod) << std::endl;
		std::cout << "AS: " << A(sFace->Pe, data->disMethod) << std::endl;
		std::cout << "--- Calc Scalar ---" << std::endl;
		std::cout << "DE: " << eFace->D << std::endl;
		std::cout << "DW: " << wFace->D << std::endl;
		std::cout << "DN: " << nFace->D << std::endl;
		std::cout << "DS: " << sFace->D << std::endl;
		std::cout << "fE: " << -eFace->f * eFace->area << std::endl;
		std::cout << "fW: " << wFace->f * wFace->area << std::endl;
		std::cout << "fN: " << -nFace->f * nFace->area << std::endl;
		std::cout << "fS: " << sFace->f * sFace->area << std::endl;

		curFace->a[E] = eFace->D * eFace->area * A(eFace->Pe, data->disMethod) + std::max(-eFace->f * eFace->area, 0.0);
		curFace->a[W] = wFace->D * wFace->area * A(wFace->Pe, data->disMethod) + std::max(wFace->f * wFace->area, 0.0);
		curFace->a[N] = nFace->D * nFace->area * A(nFace->Pe, data->disMethod) + std::max(-nFace->f * nFace->area, 0.0);
		curFace->a[S] = sFace->D * sFace->area * A(sFace->Pe, data->disMethod) + std::max(sFace->f * sFace->area, 0.0);

		curFace->a[PP] = (data->rho * curFace->area) / data->dt;
		curFace->a[PP_Tilde] = curFace->a[E] + curFace->a[W] + curFace->a[N] + curFace->a[S] + curFace->a[PP];

		std::cout << "aE: " << curFace->a[E] << std::endl;
		std::cout << "aW: " << curFace->a[W] << std::endl;
		std::cout << "aN: " << curFace->a[N] << std::endl;
		std::cout << "aS: " << curFace->a[S] << std::endl;
	}
}

void calcScalar(sCell *curCell, sData *data)
{
	curCell->b = curCell->a[PP] * curCell->sc;
	curCell->sc = (1 / curCell->a[PP_Tilde]) * (curCell->a[E] * curCell->neighCells[E]->sc + curCell->a[W] * curCell->neighCells[W]->sc + curCell->a[N] * curCell->neighCells[N]->sc + curCell->a[S] * curCell->neighCells[S]->sc + curCell->b);
}

//------------------------------------------------------
void calcFlux(sData *data)
{
	static sFace *curFace = 0;

	for (int fId = 0; fId < data->nFaces; fId++)
	{
		curFace = &data->faces[fId];

		// FIXME M2
		// compute numerical flux over each face
		curFace->f = 0;
		if (curFace->u != 0 && curFace->dy > 0)
		{
			curFace->f = data->rho * curFace->u;
		}
		else if (curFace->v != 0 && curFace->dx > 0)
		{
			curFace->f = data->rho * curFace->v;
		}

		if (!(curFace->D == 0))
		{
			curFace->Pe = curFace->f / curFace->D;
		}
		else
		{
			curFace->Pe = 1;
		}

		// Outer faces
		if (curFace->bType_u == DIRICHLET)
		{
		}

		else if (curFace->bType_u == NEUMANN)
		{
		}

		// Solid cells
		else if (curFace->bType_u == SOLID)
		{
			curFace->f = 0;
			curFace->Pe = 0;
		}
	}
}

/*
void precomputeMomentumCoeffs(sData *data, double dt)
{
	static sFace *curFace = 0;

	for (int fId = 0; fId < data->nFaces; fId++)
	{
		curFace = &data->faces[fId];

		// FIXME M3
		// precompute a_i for faces
		sCell *cellP = curFace->neighCells[M];
		sCell *cellN = cellP->neighCells[N];
		sCell *cellS = cellP->neighCells[S];
		sCell *cellW = cellP->neighCells[W];
		sCell *cellE = cellP->neighCells[E];

		if (curFace->dy != 0)
		{
			curFace->a_tilde = (1 / curFace->u) * (cellP->a[E] * cellE->faces[E]->u + cellP->a[W] * cellW->faces[E]->u + cellP->a[N] * cellN->faces[E]->u + cellP->a[S] * cellS->faces[E]->u + cellP->b + ((cellP->p - cellE->p) / cellP->dx) * cellP->dx * cellP->dy);
		}
		else if (curFace->dx != 0)
		{
			curFace->a_tilde = (1 / curFace->v) * (cellP->a[E] * cellE->faces[N]->v + cellP->a[W] * cellW->faces[N]->v + cellP->a[N] * cellN->faces[N]->v + cellP->a[S] * cellS->faces[N]->v + cellP->b + ((cellP->p - cellN->p) / cellP->dy) * cellP->dx * cellP->dy);
		}
	}
}
*/
/*
void calcMomentum(sData *data, double dt)
{
	static sFace *curFace = 0;
	double curMaxResidual = MAXDOUBLE;
	double curResidual = MAXDOUBLE;
	int curIter = 0;
	//while(curIter<data->maxIter && ABS( curMaxResidual ) > data->residual ) {
	while (ABS(curMaxResidual) > data->residual)
	{
		curIter++;
		curMaxResidual = 0;

		for (int fId = 0; fId < data->nFaces; fId++)
		{
			curFace = &data->faces[fId];

			// FIXME M3
			// calculate face velocities from momentum equation

			sCell *cellP = curFace->neighCells[M];
			sCell *cellN = cellP->neighCells[N];
			sCell *cellS = cellP->neighCells[S];
			sCell *cellW = cellP->neighCells[W];
			sCell *cellE = cellP->neighCells[E];

			if (curFace->bType_u == SOLID)
			{
				curFace->u = 0;
				curFace->v = 0;
			}
			else if (curFace->dy != 0)
			{
				//sFace* faceN = cellP->neighCells[N]->faces[E];
				//sFace* faceS = cellP->neighCells[S]->faces[E];
				//sFace* faceE = cellP->neighCells[E]->faces[E];
				//sFace* faceW = cellP->neighCells[W]->faces[E];

				curFace->u = (1 / cellP->a[PP_Tilde]) * (cellP->a[E] * cellE->faces[E]->u + cellP->a[W] * cellW->faces[E]->u + cellP->a[N] * cellN->neighCells[N]->faces[E]->u + cellP->a[S] * cellS->neighCells[S]->faces[E]->u + cellP->b + ((cellP->p - cellE->p) / cellP->dx) * cellP->dx * cellP->dy);
			}
			else if (curFace->dx != 0)
			{
				curFace->v = (1 / cellP->a[PP_Tilde]) * (cellP->a[E] * cellE->faces[N]->v + cellP->a[W] * cellW->faces[N]->v + cellP->a[N] * cellN->faces[N]->v + cellP->a[S] * cellS->faces[N]->v + cellP->b + ((cellP->p - cellN->p) / cellP->dy) * cellP->dx * cellP->dy);
			}

			if (ABS(curResidual) > ABS(curMaxResidual))
				curMaxResidual = ABS(curResidual);
		}

		calcPCorr(data, data->dt);
		vCorrections(data);
	}
}
*/
/*
double calcPCorr(sData *data, double dt)
{
	static sCell *curCell = 0;
	double curMaxResidual = MAXDOUBLE;
	double curResidual = MAXDOUBLE;
	double curMassErr = 0;
	int curIter = 0;

	while (ABS(curMaxResidual) > data->residual)
	{

		curIter++;
		curMaxResidual = 0;

		for (int cId = 0; cId < data->nCells; cId++)
		{
			curCell = &data->cells[cId];

			if (curCell->bType_p != INNERCELL)
			{
				continue;
			}

			// FIXME M4
			// calculate pressure correction
			double A;
			sCell *cellN = curCell->neighCells[N];
			sCell *cellS = curCell->neighCells[S];
			sCell *cellW = curCell->neighCells[W];
			sCell *cellE = curCell->neighCells[E];

			sFace *faceN = curCell->faces[N];
			sFace *faceS = curCell->faces[S];
			sFace *faceE = curCell->faces[E];
			sFace *faceW = curCell->faces[W];

			// Pressure Momentum Coefficients
			curCell->a_p[N] = ((curCell->rho * faceN->dx) / faceN->a_tilde) * faceN->dx;
			curCell->a_p[S] = ((curCell->rho * faceS->dx) / faceS->a_tilde) * faceS->dx;
			curCell->a_p[E] = ((curCell->rho * faceE->dy) / faceE->a_tilde) * faceE->dy;
			curCell->a_p[W] = ((curCell->rho * faceW->dy) / faceW->a_tilde) * faceW->dy;
			curCell->a_p[PP_Tilde] = curCell->a_p[E] + curCell->a_p[W] + curCell->a_p[N] + curCell->a_p[S];

			curCell->b_p = curCell->rho * ((faceW->u * faceW->dy - faceE->u * faceE->dy) + (faceS->v * faceS->dx - faceN->v * faceN->dx));

			curCell->pCorr = (1 / curCell->a_p[PP_Tilde]) * (curCell->a_p[E] * cellE->pCorr + curCell->a_p[W] * cellW->pCorr + curCell->a_p[N] * cellN->pCorr + curCell->a_p[S] * cellS->pCorr + curCell->b_p);
			curCell->p += curCell->pCorr;

			if (ABS(curResidual) > ABS(curMaxResidual))
				curMaxResidual = ABS(curResidual);
		}
	}
	return curMassErr;
}
*/
/*
void vCorrections(sData *data)
{
	static sFace *curFace = 0;

	for (int fId = 0; fId < data->nFaces; fId++)
	{
		curFace = &data->faces[fId];

		sCell *cellP = curFace->neighCells[M];
		sCell *cellN = cellP->neighCells[N];
		sCell *cellS = cellP->neighCells[S];
		sCell *cellW = cellP->neighCells[W];
		sCell *cellE = cellP->neighCells[E];

		if (curFace->bType_u == SOLID)
		{
			curFace->u = 0;
			curFace->v = 0;
		}
		else if (curFace->dy != 0)
		{
			curFace->u += (1 / cellP->a[PP_Tilde]) * (((cellP->pCorr - cellP->neighCells[E]->pCorr) / cellP->dx) * cellP->dx * cellP->dy);
		}
		else if (curFace->dx != 0)
		{
			curFace->v += (1 / cellP->a[PP_Tilde]) * (((cellP->pCorr - cellP->neighCells[N]->pCorr) / cellP->dy) * cellP->dx * cellP->dy);
		}
	}
}
*/

void simpleAlgorithm(sData *data)
{
	int curIter = 0;
	double curResidual = MAXDOUBLE;
	sCell *curCell = NULL;

	for (int cId = 0; cId < data->nCells; cId++)
	{
		curCell = &data->cells[cId];
		curCell->pCorr = 0;
	}

	while (curResidual > data->residual && curIter <= data->maxIter)
	{
		curResidual = 0;
		sFace *curFace = NULL;
		sFace *eFace = NULL;
		sFace *wFace = NULL;
		sFace *nFace = NULL;
		sFace *sFace = NULL;
		sCell *eCell = NULL;
		sCell *wCell = NULL;
		sCell *nCell = NULL;
		sCell *sCell = NULL;

		// Step 2: Estimate Velocities
		computeFaceCoeffs(data);
		for (int fId = 0; fId < data->nFaces; fId++)
		{
			curFace = &data->faces[fId];

			if (curFace->bType_u != DIRICHLET)
			{
				continue;
			}

			if (curFace->dy != 0)
			{
				eFace = curFace->neighCells[P]->faces[E];
				wFace = curFace->neighCells[M]->faces[W];
				nFace = curFace->neighCells[M]->neighCells[N]->faces[E];
				sFace = curFace->neighCells[M]->neighCells[S]->faces[E];

				curFace->v = 0;
				curFace->b = curFace->a[PP] * curFace->u;
				curFace->u = (curFace->a[E] * eFace->u + curFace->a[W] * wFace->u + curFace->a[N] * nFace->u + curFace->a[S] * sFace->u + curFace->b + (curFace->neighCells[M]->p - curFace->neighCells[P]->p) * curFace->area) / curFace->a[PP_Tilde];
			}
			else if (curFace->dx != 0)
			{
				eFace = curFace->neighCells[M]->neighCells[E]->faces[N];
				wFace = curFace->neighCells[M]->neighCells[W]->faces[N];
				nFace = curFace->neighCells[P]->faces[N];
				sFace = curFace->neighCells[M]->faces[S];

				curFace->u = 0;
				curFace->b = curFace->a[PP] * curFace->v;
				curFace->v = (curFace->a[E] * eFace->v + curFace->a[W] * wFace->v + curFace->a[N] * nFace->v + curFace->a[S] * sFace->v + curFace->b + (curFace->neighCells[M]->p - curFace->neighCells[P]->p) * curFace->area) / curFace->a[PP_Tilde];
			}
		}

		// Step 3: Compute Pressure Correction
		computePressureCoeffs(data);

		for (int cId = 0; cId < data->nCells; cId++)
		{
			curCell = &data->cells[cId];

			if (curCell->bType_sc != INNERCELL)
			{
				continue;
			}

			curCell->pCorr = (curCell->a_p[E] * curCell->neighCells[E]->pCorr + curCell->a_p[W] * curCell->neighCells[W]->pCorr + curCell->a_p[N] * curCell->neighCells[N]->pCorr + curCell->a_p[S] * curCell->neighCells[S]->pCorr + curCell->b_p) / curCell->a_p[PP_Tilde];

			if (curCell->pCorr > curResidual)
			{
				curResidual = curCell->pCorr;
			}
		}

		// Step 4: Compute Corrected Pressure
		for (int cId = 0; cId < data->nCells; cId++)
		{
			curCell = &data->cells[cId];
			curCell->p = curCell->pCorr + curCell->p;
		}

		// Step 5: Computed Corrected Velocities
		for (int cId = 0; cId < data->nCells; cId++)
		{
			curCell = &data->cells[cId];

			if (curCell->bType_sc != INNERCELL)
			{
				continue;
			}

			eCell = curCell->neighCells[E];
			wCell = curCell->neighCells[W];
			nCell = curCell->neighCells[N];
			sCell = curCell->neighCells[S];
			eFace = curCell->faces[E];
			wFace = curCell->faces[W];
			nFace = curCell->faces[N];
			sFace = curCell->faces[S];

			eFace->u = eFace->u + (curCell->pCorr - eCell->pCorr) * eFace->area / eFace->a[PP_Tilde];
			wFace->u = wFace->u + (wCell->pCorr - curCell->pCorr) * wFace->area / wFace->a[PP_Tilde];
			nFace->v = nFace->v + (curCell->pCorr - nCell->pCorr) * nFace->area / nFace->a[PP_Tilde];
			sFace->v = sFace->v + (sCell->pCorr - curCell->pCorr) * sFace->area / sFace->a[PP_Tilde];
			std::cout << "uE: " << eFace->u << std::endl;
			std::cout << "uW: " << wFace->u << std::endl;
			std::cout << "vN: " << nFace->v << std::endl;
			std::cout << "vS: " << sFace->v << std::endl;
		}
	}

	// Step 6: Solve Transport Equation
	calcFlux(data);
	computeCellCoeffs(data);
	for (int cellId = 0; cellId < data->nCells; cellId++)
	{
		curCell = &data->cells[cellId];
		if (curCell->bType_sc == INNERCELL)
		{
			std::cout << "Cell: " << curCell->id << std::endl;
			calcScalar(curCell, data);

			std::cout << "Value: " << curCell->sc << std::endl;
		}
	}
}

void computePressureCoeffs(sData *data)
{
	sCell *curCell = NULL;
	sFace *faceE = NULL;
	sFace *faceW = NULL;
	sFace *faceN = NULL;
	sFace *faceS = NULL;

	for (int cId = 0; cId < data->nCells; cId++)
	{
		curCell = &data->cells[cId];

		if (curCell->bType_sc != INNERCELL)
		{
			continue;
		}

		faceE = curCell->faces[E];
		faceW = curCell->faces[W];
		faceN = curCell->faces[N];
		faceS = curCell->faces[S];

		curCell->a_p[E] = (data->rho * faceE->area / faceE->a[PP_Tilde]) * curCell->dy;
		curCell->a_p[W] = (data->rho * faceW->area / faceW->a[PP_Tilde]) * curCell->dy;
		curCell->a_p[N] = (data->rho * faceN->area / faceN->a[PP_Tilde]) * curCell->dx;
		curCell->a_p[S] = (data->rho * faceS->area / faceS->a[PP_Tilde]) * curCell->dx;

		curCell->a_p[PP_Tilde] = curCell->a_p[E] + curCell->a_p[W] + curCell->a_p[N] + curCell->a_p[S];
		curCell->b_p = curCell->rho * ((faceW->u * faceW->area - faceE->u * faceE->area) + (faceS->v * faceS->area - faceN->v * faceN->area));
	}
}