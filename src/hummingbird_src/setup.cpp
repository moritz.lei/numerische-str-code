/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   jens.henrik.goebbert@itv.rwth-aachen.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <math.h>
#include <iostream>

#include "setup.h"
#include "data.h"
#include "metrics.h"
#include "test.h"

//-----------------------------------------------------
bool setup(sData *data)
{
   std::cout << "\nSetup:\n-------\n";
   
   /*   **********************   */
   /*         Grid Setup         */
   /*   **********************   */
   if (data->curved == AEQUIDISTANT)
   {
      Setup::aequidistantGrid(data);
   }
   else if (data->curved == CURVED)
   {
      Setup::curvedGrid(data);
   }

   /*   *********************   */
   /*       Scalar Values       */
   /*   *********************   */
   Setup::scalarGrid(data);

   /*   *********************   */
   /*        Derivatives        */
   /*   *********************   */
   if (data->curved == CURVED)
   {
      Setup::curvedDerivatives(data);
   }

   /*   ********************   */
   /*   Algebraic Parameters   */
   /*   ********************   */
   // Phi(i,j) = ( c1*Phi(i-1,j-1) + c2*Phi(i-1,j) + c3*Phi(i-1,j+1) + c4*Phi(i,j-1) + c5*Phi(i,j+1) + c6*Phi(i+1,j-1) + c7*Phi(i+1,j) + c8*Phi(i+1,j+1) ) / z
   if (data->curved == AEQUIDISTANT)
   {
      for (int i = 0; i < data->nX; i++)
      {
         for (int j = 0; j < data->nY; j++)
         {
            data->c1[i][j] = 0;                                                // i-1, j-1
            data->c2[i][j] = pow(data->deltaY, 2);                             // i-1, j
            data->c3[i][j] = 0;                                                // i-1, j+1
            data->c4[i][j] = pow(data->deltaX, 2);                             // i  , j-1
            data->c5[i][j] = pow(data->deltaX, 2);                             // i  , j+1
            data->c6[i][j] = 0;                                                // i+1, j-1
            data->c7[i][j] = pow(data->deltaY, 2);                             // i+1, j
            data->c8[i][j] = 0;                                                // i+1, j+1
            data->z[i][j] = 2 * (pow(data->deltaX, 2) + pow(data->deltaY, 2)); // denominator
         }
      }
   }
   else if (data->curved == CURVED)
   {
      double deltaXi = 1;
      double deltaEta = 1;

      for (int i = 0; i < data->nX; i++)
      {
         for (int j = 0; j < data->nY; j++)
         {
            data->c1[i][j] = data->alpha3[i][j] / (4 * deltaXi * deltaEta);                                         // i-1, j-1
            data->c2[i][j] = (data->alpha1[i][j] / pow(deltaXi, 2)) - (data->alpha4[i][j] / (2 * deltaXi));         // i-1, j
            data->c3[i][j] = -data->alpha3[i][j] / (4 * deltaXi * deltaEta);                                        // i-1, j+1
            data->c4[i][j] = (data->alpha2[i][j] / pow(deltaEta, 2)) - (data->alpha5[i][j] / (2 * deltaEta));       // i  , j-1
            data->c5[i][j] = (data->alpha2[i][j] / pow(deltaEta, 2)) + (data->alpha5[i][j] / (2 * deltaEta));       // i  , j+1
            data->c6[i][j] = -data->alpha3[i][j] / (4 * deltaXi * deltaEta);                                        // i+1, j-1
            data->c7[i][j] = (data->alpha1[i][j] / pow(deltaXi, 2)) + (data->alpha4[i][j] / (2 * deltaXi));         // i+1, j
            data->c8[i][j] = data->alpha3[i][j] / (4 * deltaXi * deltaEta);                                         // i+1, j+1
            data->z[i][j] = 2 * ((data->alpha1[i][j] / pow(deltaXi, 2)) + (data->alpha2[i][j] / pow(deltaEta, 2))); // denominator
         }
      }
   }

   /*   **************************   */
   /*   Static Boundary Conditions   */
   /*   **************************   */
   // Parallel Flow
   if (data->potentialFunc == PARALLELFLOW)
   {
      Setup::dirichletBoundariesAequidistantParallel(data);
   }

   // Stagnant Flow
   else if (data->potentialFunc == STAGNANTFLOW)
   {
      // Dirichlet West
      for (int j = 0; j < data->nY; j++)
      {
         data->Phi[0][j] = data->a * (pow(data->x[0][j], 2) - pow(data->y[0][j], 2));
         data->bPhi[0][j] = data->Phi[0][j];
         data->Psi[0][j] = 2 * data->a * data->x[0][j] * data->y[0][j];
         data->bPsi[0][j] = data->Psi[0][j];
      }

      // Dirichlet East
      for (int j = 0; j < data->nY; j++)
      {
         data->Phi[data->nX - 1][j] = data->a * (pow(data->x[data->nX - 1][j], 2) - pow(data->y[data->nX - 1][j], 2));
         data->bPhi[data->nX - 1][j] = data->Phi[data->nX - 1][j];
         data->Psi[data->nX - 1][j] = 2 * data->a * data->x[data->nX - 1][j] * data->y[data->nX - 1][j];
         data->bPsi[data->nX - 1][j] = data->Psi[data->nX - 1][j];
      }

      // Dirichlet North
      for (int i = 0; i < data->nY; i++)
      {
         data->Phi[i][data->nY - 1] = data->a * (pow(data->x[i][data->nY - 1], 2) - pow(data->y[i][data->nY - 1], 2));
         data->bPhi[i][data->nY - 1] = data->Phi[i][data->nY - 1];
         data->Psi[i][data->nY - 1] = 2 * data->a * data->x[i][data->nY - 1] * data->y[i][data->nY - 1];
         data->bPsi[i][data->nY - 1] = data->Psi[i][data->nY - 1];
      }

      // Dirichlet South
      for (int i = 0; i < data->nY; i++)
      {
         data->Phi[i][0] = data->a * (pow(data->x[i][0], 2) - pow(data->y[i][0], 2));
         data->bPhi[i][0] = data->Phi[i][0];
         data->Psi[i][0] = 2 * data->a * data->x[i][0] * data->y[i][0];
         data->bPsi[i][0] = data->Psi[i][0];
      }
   }

   std::cout << "Phi" << std::endl;
   Test::vPrintGrid(data->Phi);
   std::cout << "bPhi" << std::endl;
   Test::vPrintGrid(data->bPhi);

   return true;
}

/*   *********************   */
/*         Functions         */
/*   *********************   */

void Setup::aequidistantGrid(sData *data)
{
   for (int i = 0; i < data->nX; i++)
   {
      for (int j = 0; j < data->nY; j++)
      {
         data->x[i][j] = data->xMin + i * data->deltaX;
         data->y[i][j] = data->yMin + j * data->deltaY;
      }
   }
}

void Setup::curvedGrid(sData *data)
{
   for (int i = 0; i < data->nX; i++)
   {
      for (int j = 0; j < data->nY; j++)
      {
         data->xi[i][j] = i;
         data->eta[i][j] = j;
         data->x[i][j] = X(data, data->xi[i][j], data->eta[i][j]);
         data->y[i][j] = Y(data, data->xi[i][j], data->eta[i][j]);
      }
   }
}

void Setup::scalarGrid(sData* data)
{
   for (int i = 0; i < data->nX; i++)
   {
      for (int j = 0; j < data->nY; j++)
      {
         data->Phi[i][j] = data->defaultValue; // last iterations value
         data->Psi[i][j] = data->defaultValue; // last iterations value
         data->bPhi[i][j] = data->Phi[i][j];   // updated value
         data->bPsi[i][j] = data->Psi[i][j];   // updated value
      }
   }
}

/*   *********************   */
/*        Derivatives        */
/*   *********************   */
double Setup::firstDerivative(double p, double m, double d)
{
   return (p - m) / (2*d);
}

double Setup::secondDerivative(double p, double x, double m, double d)
{
   return (p - 2 * x + m) / pow(d, 2);
}

void Setup::curvedDerivatives(sData* data)
{
   for (int i = 0; i < data->nX; i++)
      {
         for (int j = 0; j < data->nY; j++)
         {
            double x = data->x[i][j];     // Center x value
            double y = data->y[i][j];     // Center y value
            double xi = data->xi[i][j];   // Center xi value
            std::cout << "xi: " << xi << std::endl;
            std::cout << "x: " << x << std::endl;
            double eta = data->eta[i][j]; // Center eta value
            double deltaXi = 1;           // Stepsize in xi direction
            double deltaEta = 1;          // Stepsize in eta direction

            // Fixed Distances
            double dX = data->deltaX / 1;   // Stepsize in x direction
            double xP = data->x[i][j] + dX; // East x value
            std::cout << "xP: " << xP << std::endl;
            double xM = data->x[i][j] - dX; // West x value
            std::cout << "xM: " << xM << std::endl;
            double dY = data->deltaY / 1;   // Stepsize in y direction
            double yP = data->y[i][j] + dY; // North y value
            double yM = data->y[i][j] - dY; // South y value

            double xiPx = Xi(data, xP, y);  // East xi value
            std::cout << "xiPx: " << xiPx << std::endl;
            double xiMx = Xi(data, xM, y);  // West xi value
            std::cout << "xiMx: " << xiMx << std::endl << std::endl;
            double xiPy = Xi(data, x, yP);  // North xi value
            double xiMy = Xi(data, x, yM);  // South xi value
            double etaPx = Et(data, xP, y); // East eta value
            double etaMx = Et(data, xM, y); // West eta value
            double etaPy = Et(data, x, yP); // North eta value
            double etaMy = Et(data, x, yM); // South eta value

            /*// Grid Distances
            double xP = data->x[i+1][j];              // East x value
            double xM = data->x[i-1][j];              // West x value
            double dX = 0.5 * (xP - xM);              // Stepsize in x direction
            double yP = data->y[i][j+1];              // North y value
            double yM = data->y[i][j-1];              // South y value
            double dY = 0.5 * (yP - yM);              // Stepsize in y direction

            double xiPx = data->xi[i+1][j];           // East xi value
            double xiMx = data->xi[i-1][j];           // West xi value
            double xiPy = data->xi[i][j+1];           // North xi value
            double xiMy = data->xi[i][j-1];           // South xi value
            double etaPx = data->eta[i+1][j];         // East eta value
            double etaMx = data->eta[i-1][j];         // West eta value
            double etaPy = data->eta[i][j+1];         // North eta value
            double etaMy = data->eta[i][j-1];         // South eta value */

            // Xi Derivatives
            data->dXidX[i][j] = firstDerivative(xiPx, xiMx, dX);
            data->dXidY[i][j] = firstDerivative(xiPy, xiMy, dY);
            data->d2XidX2[i][j] = secondDerivative(xiPx, xi, xiMx, dX);
            data->d2XidY2[i][j] = secondDerivative(xiPy, xi, xiMy, dY);

            // Eta Derivatives
            data->dEtadX[i][j] = firstDerivative(etaPx, etaMx, dX);
            data->dEtadY[i][j] = firstDerivative(etaPy, etaMy, dY);
            data->d2EtadX2[i][j] = secondDerivative(etaPx, eta, etaMx, dX);
            data->d2EtadY2[i][j] = secondDerivative(etaPy, eta, etaMy, dY);

            // Contour Derivatives
            data->dhdxMin[i][j] = firstDerivative(ContourMin(xP), ContourMin(xM), dX);
            data->dhdxMax[i][j] = firstDerivative(ContourMax(xP), ContourMax(xM), dX);

            // Curved Derivative Parameters
            // d2Phi(i,j)dX2 + d2Phi(i,j)dY2 = 0 = a1 * d2Phi(i,j)dXi2 + a2 * d2Phi(i,j)dEta2 + a3 * d2Phi(i,j)dXidEta + a4 * dPhi(i,j)dXi + a5 * dPhi(i,j)dEta + a6 * Phi(i,j)
            data->alpha1[i][j] = pow(data->dXidX[i][j], 2) + pow(data->dXidY[i][j], 2);                                 // d2Phi/dXi2
            data->alpha2[i][j] = pow(data->dEtadX[i][j], 2) + pow(data->dEtadY[i][j], 2);                               // d2Phi/dEta2
            data->alpha3[i][j] = 2 * (data->dXidX[i][j] * data->dEtadX[i][j] + data->dXidY[i][j] * data->dEtadY[i][j]); // d2Phi/dXidEta
            data->alpha4[i][j] = data->d2XidX2[i][j] + data->d2XidY2[i][j];                                             // dPhi/dXi
            data->alpha5[i][j] = data->d2EtadX2[i][j] + data->d2EtadY2[i][j];                                           // dPhi/dEta
            data->alpha6[i][j] = 0;                                                                                     // Constant

            // Lower Curved Boundary Parameters
            // Phi(i,j) = ( b1 * Phi(i+1,j) + b2 * Phi(i-1,j) + b3 * Phi(i,j+2) + b4 * Phi(i,j+1) ) / b5
            data->beta1Min[i][j] = (data->dhdxMin[i][j] * data->dXidX[i][j] - data->dXidY[i][j]) / (2 * deltaXi);        // i+1, j
            data->beta2Min[i][j] = -(data->dhdxMin[i][j] * data->dXidX[i][j] - data->dXidY[i][j]) / (2 * deltaXi);       // i-1, j
            data->beta3Min[i][j] = -(data->dhdxMin[i][j] * data->dEtadX[i][j] - data->dEtadY[i][j]) / (2 * deltaEta);    // i  , j+2
            data->beta4Min[i][j] = 4 * (data->dhdxMin[i][j] * data->dEtadX[i][j] - data->dEtadY[i][j]) / (2 * deltaEta); // i  , j+1
            data->beta5Min[i][j] = 3 * (data->dhdxMin[i][j] * data->dEtadX[i][j] - data->dEtadY[i][j]) / (2 * deltaEta); // denominator

            // Upper Curved Boundary Parameters
            // Phi(i,j) = ( b1 * Phi(i+1,j) + b2 * Phi(i-1,j) + b3 * Phi(i,j-2) + b4 * Phi(i,j-1) ) / b5
            data->beta1Max[i][j] = (data->dhdxMax[i][j] * data->dXidX[i][j] - data->dXidY[i][j]) / (2 * deltaXi);           // i+1, j
            data->beta2Max[i][j] = -(data->dhdxMax[i][j] * data->dXidX[i][j] - data->dXidY[i][j]) / (2 * deltaXi);          // i-1, j
            data->beta3Max[i][j] = (data->dhdxMax[i][j] * data->dEtadX[i][j] - data->dEtadY[i][j]) / (2 * deltaEta);        // i  , j-2
            data->beta4Max[i][j] = (-4) * (data->dhdxMax[i][j] * data->dEtadX[i][j] - data->dEtadY[i][j]) / (2 * deltaEta); // i  , j-1
            data->beta5Max[i][j] = -3 * (data->dhdxMax[i][j] * data->dEtadX[i][j] - data->dEtadY[i][j]) / (2 * deltaEta);   // denominator
         }
      }
}

void Setup::dirichletBoundariesAequidistantParallel(sData* data)
{
   // Dirichlet West
      for (int j = 0; j < data->nY; j++)
      {
         data->Phi[0][j] = data->uInfty * data->x[0][j] + data->vInfty * data->y[0][j];
         data->bPhi[0][j] = data->Phi[0][j];
         data->Psi[0][j] = -data->vInfty * data->x[0][j] + data->uInfty * data->y[0][j];
         data->bPsi[0][j] = data->Psi[0][j];
      }

      // Dirichlet East
      for (int j = 0; j < data->nY; j++)
      {
         data->Phi[data->nX - 1][j] = data->uInfty * data->x[data->nX - 1][j] + data->vInfty * data->y[data->nX - 1][j];
         data->bPhi[data->nX - 1][j] = data->Phi[data->nX - 1][j];
         data->Psi[data->nX - 1][j] = -data->vInfty * data->x[data->nX - 1][j] + data->uInfty * data->y[data->nX - 1][j];
         data->bPsi[data->nX - 1][j] = data->Psi[data->nX - 1][j];
      }

      // Dirichlet North
      for (int i = 0; i < data->nX; i++)
      {
         data->Phi[i][data->nY - 1] = data->uInfty * data->x[i][data->nY - 1] + data->vInfty * data->y[i][data->nY - 1];
         data->bPhi[i][data->nY - 1] = data->Phi[i][data->nY - 1];
         data->Psi[i][data->nY - 1] = -data->vInfty * data->x[i][data->nY - 1] + data->uInfty * data->y[i][data->nY - 1];
         data->bPsi[i][data->nY - 1] = data->Psi[i][data->nY - 1];
      }

      // Dirichlet South
      for (int i = 0; i < data->nX; i++)
      {
         data->Phi[i][0] = data->uInfty * data->x[i][0] + data->vInfty * data->y[i][0];
         data->bPhi[i][0] = data->Phi[i][0];
         data->Psi[i][0] = -data->vInfty * data->x[i][0] + data->uInfty * data->y[i][0];
         data->bPsi[i][0] = data->Psi[i][0];
      }
}