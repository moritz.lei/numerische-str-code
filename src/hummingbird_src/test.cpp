#include "test.h"
#include <iostream>

Test::Test()
{
    this->uInf = 2;
    this->vInf = 0;

    gridSetup();
}

void Test::gridSetup()
{
    testGridX = arraySetup();
    testGridY = arraySetup();
    testGridScalar = arraySetup();
    inputGridScalar = arraySetup();
    resultGridScalar = arraySetup();

    // X
    testGridX[0][0] = 0;
    testGridX[0][1] = 0;
    testGridX[0][2] = 0;
    testGridX[1][0] = 0.5;
    testGridX[1][1] = 0.5;
    testGridX[1][2] = 0.5;
    testGridX[2][0] = 1;
    testGridX[2][1] = 1;
    testGridX[2][2] = 1;

    // Y
    testGridY[0][0] = 0;
    testGridY[0][1] = 0.5;
    testGridY[0][2] = 1;
    testGridY[1][0] = 0;
    testGridY[1][1] = 0.5;
    testGridY[1][2] = 1;
    testGridY[2][0] = 0;
    testGridY[2][1] = 0.5;
    testGridY[2][2] = 1;

    // Scalar - Parallel Flow: Phi = uInf * x + vInf * y
    testGridScalar[0][0] = uInf * testGridX[0][0] + vInf * testGridY[0][0];
    testGridScalar[0][1] = uInf * testGridX[0][1] + vInf * testGridY[0][1];
    testGridScalar[0][2] = uInf * testGridX[0][2] + vInf * testGridY[0][2];
    testGridScalar[1][0] = uInf * testGridX[1][0] + vInf * testGridY[1][0];
    testGridScalar[1][1] = uInf * testGridX[1][1] + vInf * testGridY[1][1];
    testGridScalar[1][2] = uInf * testGridX[1][2] + vInf * testGridY[1][2];
    testGridScalar[2][0] = uInf * testGridX[2][0] + vInf * testGridY[2][0];
    testGridScalar[2][1] = uInf * testGridX[2][1] + vInf * testGridY[2][1];
    testGridScalar[2][2] = uInf * testGridX[2][2] + vInf * testGridY[2][2];

    // Scalar Input
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            inputGridScalar[i][j] = testGridScalar[i][j];
        }
    }
    inputGridScalar[1][1] = 42;
}

double** Test::arraySetup()
{
    double** arr = new double*[3];
    for (int i = 0; i < 3; i++)
    {
        arr[i] = new double[3];
    }

    return arr;
}

void Test::printGrid(double** grid)
{
    std::cout << "Y" << std::endl;
    for (int j = 2; j >= 0; j--)
    {
        std::cout << testGridY[0][j] << "\t | ";
        for (int i = 0; i < 3; i++)
        {
            std::cout << grid[i][j] << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << "X \t   " << testGridX[0][0] << "\t" << testGridX[1][0] << "\t" << testGridX[2][0] << std::endl;
}

void Test::vPrintGrid(double** grid)
{
    std::cout << "Y" << std::endl;
    for (int j = 2; j >= 0; j--)
    {
        std::cout << j << "\t | ";
        for (int i = 0; i < 3; i++)
        {
            std::cout << grid[i][j] << "\t";
        }
        std::cout << std::endl;
    }
    std::cout << "X \t   " << 0 << "\t" << 1 << "\t" << 2 << std::endl;
}

void Test::run(sData* data)
{
    Setup::dirichletBoundariesAequidistantParallel(data);
}