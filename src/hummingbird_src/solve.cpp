/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   jens.henrik.goebbert@itv.rwth-aachen.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <math.h>
#include <iostream>

#include "solve.h"
#include "data.h"
#include "setup.h"
#include "test.h"

//-----------------------------------------------------
bool solve( sData* data ) {
   std::cout << "\nSolve:\n-------\n";

   if( !gaussseidel( data,data->Phi, data->bPhi) )	{
      return false;
   }
   /*
   if( !gaussseidel( data,data->Psi, data->bPsi) )	{
      return false;
   }
   */
   return true;
}

//--- Gauss-Seidel solver -----------------------------
bool gaussseidel( sData* data, double** s, double** b) {
   int curIter = 0;
   double curResidual    = MAXDOUBLE;
   double curMaxResidual = MAXDOUBLE;
   
   std::cout << "\r\tRunning Gauss-Seidel solver... ";
   std::cout << std::endl;
   while( curIter<data->maxIter && ABS( curMaxResidual ) > data->maxResidual ) {
      curIter++;
      curMaxResidual = 0;

      for (int i = 1; i < data->nX - 1; i++)
      {

         /*   *******************************   */
         /*     Dynamic Boundary Conditions     */
         /*   *******************************   */

         // Neumann South
         if (data->curved == CURVED)
         {
            int j = 0;
            b[i][j] = (data->beta1Min[i][j] * s[i+1][j]
                           + data->beta2Min[i][j] * s[i-1][j]
                           + data->beta3Min[i][j] * s[i][j+2]
                           + data->beta4Min[i][j] * s[i][j+1])
                           / data->beta5Min[i][j];

            curResidual = b[i][j] - s[i][j];               
            b[i][j] = s[i][j] + data->relaxation * curResidual;
         }

         // Neumann North
         if (data->curved == CURVED)
         {
            int j = data->nY - 1;
            b[i][j] = (data->beta1Max[i][j] * s[i+1][j]
                           + data->beta2Max[i][j] * s[i-1][j]
                           + data->beta3Max[i][j] * s[i][j-2]
                           + data->beta4Max[i][j] * s[i][j-1])
                           / data->beta5Max[i][j];

            curResidual = b[i][j] - s[i][j];
            s[i][j] = s[i][j] + data->relaxation * curResidual;
            b[i][j] = s[i][j];
         }
         
         /*   ********************   */
         /*      Grid Iteration      */
         /*   ********************   */
         for (int j = 1; j < data->nY - 1; j++)
         {
            // include already updated values
            b[i][j] = (data->c1[i][j] * b[i-1][j-1]
                        + data->c2[i][j] * b[i-1][j] 
                        + data->c3[i][j] * b[i-1][j+1]
                        + data->c4[i][j] * b[i][j-1] 
                        + data->c5[i][j] * s[i][j+1] 
                        + data->c6[i][j] * s[i+1][j-1] 
                        + data->c7[i][j] * s[i+1][j] 
                        + data->c8[i][j] * s[i+1][j+1]) 
                        / data->z[i][j];
            curResidual = b[i][j] - s[i][j];

            /*
            std::cout << std::endl << "--- xi ---" << std::endl;
            Test::vPrintGrid(data->xi);
            std::cout << std::endl << "--- eta ---" << std::endl;
            Test::vPrintGrid(data->eta);

            std::cout << std::endl << "--- x ---" << std::endl;
            Test::vPrintGrid(data->x);
            std::cout << std::endl << "--- y ---" << std::endl;
            Test::vPrintGrid(data->y);

            std::cout << std::endl << "--- dXidX ---" << std::endl;
            Test::vPrintGrid(data->dXidX);
            std::cout << std::endl << "--- dXidY ---" << std::endl;
            Test::vPrintGrid(data->dXidY);
            std::cout << std::endl << "--- d2XidX2 ---" << std::endl;
            Test::vPrintGrid(data->d2XidX2);
            std::cout << std::endl << "--- d2XidY2 ---" << std::endl;
            Test::vPrintGrid(data->d2XidY2);

            std::cout << std::endl << "--- dEtadX ---" << std::endl;
            Test::vPrintGrid(data->dEtadX);
            std::cout << std::endl << "--- dEtadY ---" << std::endl;
            Test::vPrintGrid(data->dEtadY);
            std::cout << std::endl << "--- d2XidX2 ---" << std::endl;
            Test::vPrintGrid(data->d2EtadX2);
            std::cout << std::endl << "--- d2EtadY2 ---" << std::endl;
            Test::vPrintGrid(data->d2EtadY2);

            std::cout << "alpha 1: " << data->alpha1[i][j] << std::endl;
            std::cout << "alpha 2: " << data->alpha2[i][j] << std::endl;
            std::cout << "alpha 3: " << data->alpha3[i][j] << std::endl;
            std::cout << "alpha 4: " << data->alpha4[i][j] << std::endl;
            std::cout << "alpha 5: " << data->alpha5[i][j] << std::endl;
            std::cout << "alpha 6: " << data->alpha6[i][j] << std::endl;
            std::cout << "c1: " << data->c1[i][j] << " * " << b[i-1][j-1] << std::endl;
            std::cout << "c2: " << data->c2[i][j] << " * " << b[i-1][j] << std::endl;
            std::cout << "c3: " << data->c3[i][j] << " * " << b[i-1][j+1] << std::endl;
            std::cout << "c4: " << data->c4[i][j] << " * " << b[i][j-1] << std::endl;
            std::cout << "c5: " << data->c5[i][j] << " * " << s[i][j+1] << std::endl;
            std::cout << "c6: " << data->c6[i][j] << " * " << s[i+1][j-1] << std::endl;
            std::cout << "c7: " << data->c7[i][j] << " * " << s[i+1][j] << std::endl;
            std::cout << "c8: " << data->c8[i][j] << " * " << s[i+1][j+1] << std::endl;

            std::cout << "Former Value: " << s[i][j] << std::endl;
            std::cout << "New Value: " << b[i][j] << std::endl;
            std::cout << "Residuum: " << curResidual << std::endl;
            */

            b[i][j] = s[i][j] + data->relaxation * curResidual;
            //std::cout << "b: " << b[i][j] << ", s: " << s[i][j] << ", r: " << curResidual << std::endl;
            if (ABS(curResidual) > ABS(curMaxResidual))
               curMaxResidual = ABS(curResidual);
         }

      }

      /*   *************************   */
      /*      Scalar Value Update      */
      /*   *************************   */
      for (int i = 0; i < data->nX; i++)
      {
         for (int j = 0; j < data->nY; j++)
         {
            s[i][j] = b[i][j];
         }
      }


   }
   std::cout << "done." << std::endl;
   std::cout << "\n\tGauss-Seidel iterations: " << curIter << std::endl;
   std::cout << "\n\tMaximum residuum       : " << curMaxResidual << std::endl;

   return true;
}

//--- Jacobi solver -----------------------------------
bool jacobi( sData* data, double** s, double** b) {
   int curIter = 0;
   double curResidual    = MAXDOUBLE;
   double curMaxResidual = MAXDOUBLE;
   
   std::cout << "\r\tRunning Gauss-Seidel solver... ";
   while( curIter<data->maxIter && ABS( curMaxResidual ) > data->maxResidual ) {
      curIter++;

      for (int i = 1; i < data->nX - 1; i++)
      {
         for (int j = 1; j < data->nY - 1; j++)
         {
            // include only last iteration's values
            b[i][j] = (data->c1[i][j] * s[i-1][j-1] + data->c2[i][j] * s[i-1][j] + data->c3[i][j] * s[i-1][j+1] + data->c4[i][j] * s[i][j-1] + data->c5[i][j] * s[i][j+1] + data->c6[i][j] * s[i+1][j-1] + data->c7[i][j] * s[i+1][j] + data->c8[i][j] * s[i+1][j+1]) / data->z[i][j];
            curResidual = b[i][j] - s[i][j];
            if (ABS(curResidual) > ABS(curMaxResidual))
               curMaxResidual = ABS(curResidual);
         }
      }

      // Update s
      for (int i = 0; i < data->nX; i++)
      {
         for (int j = 0; j < data->nY; j++)
         {
            s[i][j] = b[i][j];
         }
      }

   }
   std::cout << "done." << std::endl;
   std::cout << "\n\tJacobi iterations: " << curIter << std::endl;
   std::cout << "\n\tMaximum residuum       : " << curMaxResidual << std::endl;
   return true;
}

//--- Thomas algorithm --------------------------------
bool thomas( sData* data, double** s ) {
   // optional
   return true;
}
