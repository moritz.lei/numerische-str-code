/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   jens.henrik.goebbert@itv.rwth-aachen.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef SETUP_H
#define SETUP_H

#include "data.h"
class Setup
{
private:
    Setup();
    static double firstDerivative(double p, double m, double d);
    static double secondDerivative(double p, double x, double m, double d);
public:
    static void aequidistantGrid(sData* data);
    static void curvedGrid(sData* data);
    static void scalarGrid(sData* data);
    static void curvedDerivatives(sData* data);
    static void dirichletBoundariesAequidistantParallel(sData* data);
};

bool setup(sData* data);

#endif
