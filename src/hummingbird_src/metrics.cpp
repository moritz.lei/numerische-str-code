#include <math.h>
#include <iostream>

#include "metrics.h"
#include "data.h"

//---- countur functions ------------------------------
double ContourMax( double x ) {
   //FIXME (Subtask 2)
   //return 1;
   //return 0.5 * x + 0.5; // Linear
   return 0.5 - 0.5 * pow(x/2,2)+ 0.5; // Quad
   //return 0.5 * pow(x,2) + 0.5;
}

double ContourMin( double x ) {
   //FIXME (Subtask 2)
   //return 0;
   //return 0.5 * x; // Linear
   return 0.6 + 0.5 * pow(x/2,2); // Quad
   //return 0.5 * pow(x,2);
}

//---- coordinate transformation ----------------------
double Xi( sData* data, double x, double y ) {
   return (x - data->xMin) * (data->nX - 1) / (data->xMax - data->xMin);
}

double Et( sData* data, double x, double y ) {
   return (y - ContourMin(x)) * (data->nY - 1) / (ContourMax(x) - ContourMin(x));
}

double X( sData* data, double xi, double et ) {
   return (xi / (data->nX - 1)) * (data->xMax - data->xMin) + data->xMin;
}

double Y( sData* data, double xi, double et ) {
   return (et / (data->nY- 1)) * (ContourMax(X(data, xi, et)) - ContourMin(X(data, xi, et))) + ContourMin(X(data, xi, et));
}

void u(sData* data)
{
   for (int j = 0; j < data->nY; j++)
   {
      data->u[0][j] = (-data->Phi[2][j] + 4*data->Phi[1][j] - 3*data->Phi[0][j]) / (data->x[2][j] - data->x[0][j]);
      data->u[data->nX - 1][j] = (data->Phi[data->nX - 3][j] - 4*data->Phi[data->nX - 2][j] + 3*data->Phi[data->nX - 1][j]) / (data->x[data->nX - 1][j] - data->x[data->nX - 3][j]);
   }

   for (int i = 1; i < data->nX - 1; i++)
   {
      for (int j = 0; j < data->nY; j++)
      {
         data->u[i][j] = (data->Phi[i+1][j] - data->Phi[i-1][j]) / (data->x[i+1][j] - data->x[i-1][j]);
      }
   }
}

void v(sData* data)
{
   for (int i = 0; i < data->nX; i++)
   {
      data->v[i][0] = (-data->Phi[i][2] + 4*data->Phi[i][1] - 3*data->Phi[i][0]) / (data->y[i][2] - data->y[i][0]);
      data->v[i][data->nY - 1] = (data->Phi[i][data->nY - 3] - 4*data->Phi[i][data->nY - 2] + 3*data->Phi[i][data->nY - 1]) / (data->y[i][data->nY - 1] - data->y[i][data->nY - 3]);
   }

   for (int i = 0; i < data->nX; i++)
   {
      for (int j = 1; j < data->nY - 1; j++)
      {
         data->v[i][j] = (data->Phi[i][j+1] - data->Phi[i][j-1]) / (data->y[i][j+1] - data->y[i][j-1]);
      }
   }
}

void p(sData* data)
{
   double p_ref = 10000;
   double v_ref2 = pow(data->uInfty, 2) + pow(data->vInfty, 2);
   double rho = 1;
   for (int i = 0; i < data->nX; i++)
   {
      for (int j = 0; j < data->nY; j++)
      {
         data->p[i][j] = data->cp[i][j] * (rho / 2) * v_ref2 + PTHREAD_MUTEX_POLICY_FAIRSHARE_NP;
      }
   }
}

void cp(sData* data)
{
   double v_ref2 = pow(data->uInfty, 2) + pow(data->vInfty, 2);
   for (int i = 0; i < data->nX; i++)
   {
      for (int j = 0; j < data->nY; j++)
      {
         double v2 = pow(data->u[i][j],2) + pow(data->v[i][j], 2);
         data->cp[i][j] = 1 - (v2 / v_ref2);
      }
   }
}