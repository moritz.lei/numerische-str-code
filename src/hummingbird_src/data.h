/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   jens.henrik.goebbert@itv.rwth-aachen.de                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef DATA_H
#define DATA_H

#include <limits>

#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define ABS(x) (((x) > 0) ? (x) : -(x))

#define MAXDOUBLE (std::numeric_limits<double>::max())
#define MINDOUBLE (std::numeric_limits<double>::min())
#ifndef MAXFLOAT
#define MAXFLOAT (std::numeric_limits<float>::max())
#endif
#define MINFLOAT (std::numeric_limits<float>::min())
#define MAXINT (std::numeric_limits<int>::max())
#define MININT (std::numeric_limits<int>::min())

#define PARALLELFLOW 0
#define STAGNANTFLOW 1

#define AEQUIDISTANT 0
#define CURVED 1

#define TEST 1

struct sData
{
   sData(){};

   // test
   int test;

   // grid & geometry settings
   int nX;
   int nY;
   double xMin;
   double xMax;
   double yMin;
   double yMax;
   double deltaX; // step size in x direction
   double deltaY; // step size in y direction

   // potential flow settings
   int potentialFunc;
   double uInfty;
   double vInfty;
   double a;

   // solver settings
   int maxIter;
   double maxResidual;
   double relaxation;

   // x,y grids
   int curved;
   double **x;
   double **y;
   double **xi;
   double ** eta;

   // scalar values
   double defaultValue;
   double **Phi;
   double **Psi;

   // current iteration values
   double **bPhi;
   double **bPsi;

   // algebraic parameters
   double **c1;
   double **c2;
   double **c3;
   double **c4;
   double **c5;
   double **c6;
   double **c7;
   double **c8;
   double **z;

   // derivatives
   double **dXidX;
   double **dXidY;
   double **d2XidX2;
   double **d2XidY2;
   double **dEtadX;
   double **dEtadY;
   double **d2EtadX2;
   double **d2EtadY2;
   double **dhdxMin;
   double **dhdxMax;

   // curved derivative parameters
   double **alpha1;
   double **alpha2;
   double **alpha3;
   double **alpha4;
   double **alpha5;
   double **alpha6;

   // curved boundary parameters
   double **beta1Min;
   double **beta1Max;
   double **beta2Min;
   double **beta2Max;
   double **beta3Min;
   double **beta3Max;
   double **beta4Min;
   double **beta4Max;
   double **beta5Min;
   double **beta5Max;

   // 
   double **u;
   double **v;
   double **p;
   double **cp;
};

double **allocGrid1Mem(const sData *const data, const double preset);
void freeGrid1Mem(const sData *const data, double **mem);
double ***allocGridXMem(const sData *const data, const int vSize, const double preset);

#endif
