#ifndef TEST_H
#define TEST_H

#include "data.h"
#include "setup.h"

class Test
{
public:
    Test();
    void run(sData* data);
    static void vPrintGrid(double** grid);

private:
    double** testGridX;
    double** testGridY;
    double** testGridScalar;
    double** inputGridScalar;
    double** resultGridScalar;

    double uInf;
    double vInf;
    double deltaX;
    double deltaY;
    int nX;
    int nY;

    void gridSetup();
    void printGrid(double** grid);
    void evaluate();
    double** arraySetup();
};

#endif